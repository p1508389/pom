package Model;

public enum Orientation {
    /*
    L'orientation représente de quel côté regarde la voiture.
    (Si les feux avant sont vers le nord, la voiture est orientée vers le nord)
     */
    Nord,
    Sud,
    Est,
    Ouest
}
