package Model;

public enum Direction {
    /*
    La direction représente l'origine de la voiture et sa destination.
     */
    NS,
    NE,
    NO,

    SN,
    SE,
    SO,

    EO,
    EN,
    ES,

    OE,
    ON,
    OS,
}
