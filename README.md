# POM

<h1>Getting started</h1>
<h2>Pour exécuter le programme avec IntelliJ </h2>

1.   Ouvrir le dossier Test dans IntelliJ
2.  Puis File —> Project Structure —>  Librairie —> Le « + » —> Java —> Puis ajouter le dossier présent dans librairies/javafx-sdk-11.0.2/lib —> Apply
3.  Puis Run —> Edit Configurations —> Le « + » —> 
        Main class : VueController.main  
	    Program arguments : demo (pour la démo)  
        (Pour lancer les tests, VM options : « -ea » et Program arguments: « test »)
4.  Puis apply
5.  Puis Run —> Run « demo »
